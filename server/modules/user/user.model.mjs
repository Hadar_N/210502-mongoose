import mongoose from 'mongoose';
const { Schema, model } = mongoose;

const UserSchema = new Schema({
    first_name  : {type: String, required: true},
    last_name   : {type: String, required: true},
    email       : {type: String, required: true, unique: true},
});
  
export default model('user',UserSchema);