import Joi from 'joi';

const addSchema = Joi.object({
    first_name: Joi.string()
        .min(2)
        .max(15)
        .required(),

        last_name: Joi.string()
        .min(2)
        .max(15)
        .required(),
        
    email: Joi.string()
        .email({ minDomainSegments: 2 })
        .required()
})
const patchSchema = Joi.object({
    first_name: Joi.string()
        .min(2)
        .max(15),

        last_name: Joi.string()
        .min(2)
        .max(15),
        
    email: Joi.string()
        .email({ minDomainSegments: 2 })
}).or('first_name', 'last_name', 'email')

export const authMiddleware = (req, res, next) => {

    let valRes;
    switch (req.method) {
        case "PUT":
            valRes = patchSchema.validate(req.body);
            break;
        case "POST":
            valRes = addSchema.validate(req.body);
            break;
    }

        next(valRes.error);
}